# MUlti SSH

For running SSH commands on multiple servers.

## Usage

### Volume(s)

The `mussh` container requires a config file (described below) to exist at `/root/.mussh/config.yaml`.

This file can be mounted directly, mounted in parent directory, or writen by subsequent commands (e.g. in build system or inheriting image).

e.g.
```bash
# directly mount config
docker run -v /path/to/config:/root/.mussh/config.yaml gregbacchus/mussh
# mount directory
docker run -v ~/.mussh:/root/.mussh gregbacchus/mussh
```

It will also be necessary to mount any private keys that you reference in the config.

### Examples

See what you can do:

```bash
docker run -v ~/.mussh:/root/.mussh gregbacchus/mussh
# or
docker run -v ~/.mussh:/root/.mussh gregbacchus/mussh run --help
docker run -v ~/.mussh:/root/.mussh gregbacchus/mussh list --help
```

To run on all web servers:

```bash
docker run -v ~/.mussh:/root/.mussh gregbacchus/mussh run -t role:web "ls -lah"
```

To run on a specific server:

```bash
docker run -v ~/.mussh:/root/.mussh gregbacchus/mussh run -i app1 "ls -lah" "df -h"
```

To run a local script

```bash
docker run -v ~/.mussh:/root/.mussh gregbacchus/mussh run -t role:web -s ~/scripts/test.sh
```

## Matching

Target servers will be selected from the config file by either id(s) ot tag(s).

Specifying multipe `--id <id>` (or `-i <id>`) arguments will select the servers matching the specified ids.

Specifying multiple `--tag <tag>` (or `-t <tag>`) arguments will select only the servers that have *all of the matching tags*. (i.e. each tag argument filters down the number of target servers). E.g. to target all **web** servers in **west-us** you specify `-t web -t west-us`.

## Configuration

Config file in `/root/.mussh/config.yaml`

```yaml
auths:
  - id: default
    type: password
    username: myself
    password: super-secret
  - id: key
    type: rsa
    username: myself
    keyPath: /root/.ssh/id_rsa
servers:
  - id: web1
    hostname: web1.example.com
    authid: default
    tags:
      - role:web
      - dc:us-west-2
  - id: web2
    hostname: web2.example.com
    authid: default
    tags:
      - role:web
      - dc:us-west
  - id: app1
    hostname: app1.example.com
    port: 9990
    ip: v6
    auth:
      type: password
      username: adhoc
      password: test
    tags:
      - foo:bar
```
